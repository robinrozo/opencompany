<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Buy;
use AppBundle\Form\BuyType;

/**
 * Buy controller.
 *
 * @Route("/account/buy")
 */
class BuyController extends Controller
{
    /**
     * Lists all Buy entities.
     *
     * @Route("/", name="buy_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $buys = $em->getRepository('AppBundle:Buy')->findAll();

        return $this->render('buy/index.html.twig', array(
            'buys' => $buys,
        ));
    }

    /**
     * Creates a new Buy entity.
     *
     * @Route("/new", name="buy_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $buy = new Buy();
        $form = $this->createForm('AppBundle\Form\BuyType', $buy);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($buy);
            $em->flush();

            $this->addFlash(
                'success',
                $this->container->get('translator')->trans('main.success')
            );

            return $this->redirectToRoute('buy_show', array('id' => $buy->getId()));
        }

        return $this->render('buy/new.html.twig', array(
            'buy' => $buy,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Buy entity.
     *
     * @Route("/{id}", name="buy_show")
     * @Method("GET")
     */
    public function showAction(Buy $buy)
    {
        $deleteForm = $this->createDeleteForm($buy);

        return $this->render('buy/show.html.twig', array(
            'buy' => $buy,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Buy entity.
     *
     * @Route("/{id}/edit", name="buy_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Buy $buy)
    {
        $deleteForm = $this->createDeleteForm($buy);
        $editForm = $this->createForm('AppBundle\Form\BuyType', $buy);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($buy);
            $em->flush();

            $this->addFlash(
                'success',
                $this->container->get('translator')->trans('main.success')
            );
            
            return $this->redirectToRoute('buy_edit', array('id' => $buy->getId()));
        }

        return $this->render('buy/edit.html.twig', array(
            'buy' => $buy,
            'form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Buy entity.
     *
     * @Route("/{id}", name="buy_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Buy $buy)
    {
        $form = $this->createDeleteForm($buy);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($buy);
            $em->flush();
        }

        return $this->redirectToRoute('buy_index');
    }

    /**
     * Creates a form to delete a Buy entity.
     *
     * @param Buy $buy The Buy entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Buy $buy)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('buy_delete', array('id' => $buy->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
