<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\TaxTypeRate;
use AppBundle\Form\TaxTypeRateType;

/**
 * TaxTypeRate controller.
 *
 * @Route("/admin/taxtyperate")
 */
class TaxTypeRateController extends Controller
{
    /**
     * Lists all TaxTypeRate entities.
     *
     * @Route("/", name="taxtyperate_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $taxTypeRates = $em->getRepository('AppBundle:TaxTypeRate')->findAll();

        return $this->render('taxtyperate/index.html.twig', array(
            'taxTypeRates' => $taxTypeRates,
        ));
    }

    /**
     * Creates a new TaxTypeRate entity.
     *
     * @Route("/new", name="taxtyperate_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $taxTypeRate = new TaxTypeRate();
        $form = $this->createForm('AppBundle\Form\TaxTypeRateType', $taxTypeRate);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($taxTypeRate);
            $em->flush();

            $this->addFlash(
                'success',
                $this->container->get('translator')->trans('main.success')
            );

            return $this->redirectToRoute('taxtyperate_show', array('id' => $taxTypeRate->getId()));
        }

        return $this->render('taxtyperate/new.html.twig', array(
            'taxTypeRate' => $taxTypeRate,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a TaxTypeRate entity.
     *
     * @Route("/{id}", name="taxtyperate_show")
     * @Method("GET")
     */
    public function showAction(TaxTypeRate $taxTypeRate)
    {
        $deleteForm = $this->createDeleteForm($taxTypeRate);

        return $this->render('taxtyperate/show.html.twig', array(
            'taxTypeRate' => $taxTypeRate,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing TaxTypeRate entity.
     *
     * @Route("/{id}/edit", name="taxtyperate_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, TaxTypeRate $taxTypeRate)
    {
        $deleteForm = $this->createDeleteForm($taxTypeRate);
        $editForm = $this->createForm('AppBundle\Form\TaxTypeRateType', $taxTypeRate);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($taxTypeRate);
            $em->flush();

            $this->addFlash(
                'success',
                $this->container->get('translator')->trans('main.success')
            );

            return $this->redirectToRoute('taxtyperate_edit', array('id' => $taxTypeRate->getId()));
        }

        return $this->render('taxtyperate/edit.html.twig', array(
            'taxTypeRate' => $taxTypeRate,
            'form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a TaxTypeRate entity.
     *
     * @Route("/{id}", name="taxtyperate_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, TaxTypeRate $taxTypeRate)
    {
        $form = $this->createDeleteForm($taxTypeRate);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($taxTypeRate);
            $em->flush();
        }

        return $this->redirectToRoute('taxtyperate_index');
    }

    /**
     * Creates a form to delete a TaxTypeRate entity.
     *
     * @param TaxTypeRate $taxTypeRate The TaxTypeRate entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TaxTypeRate $taxTypeRate)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('taxtyperate_delete', array('id' => $taxTypeRate->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
