<?php

namespace AppBundle\Controller;

use FOS\UserBundle\Form\Type\ChangePasswordFormType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Profile;
use AppBundle\Form\ProfileType;

/**
 * Profile controller.
 *
 * @Route("/profile")
 */
class ProfileController extends Controller
{
    /**
     * Lists all Profile entities.
     *
     * @Route("/", name="profile_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $profile = $this->getUser()->getProfile();
        $deleteForm = $this->createDeleteForm($profile);

        return $this->render('profile/show.html.twig', array(
            'profile' => $profile,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Creates a new Profile entity.
     *
     * @Route("/new", name="profile_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $profile = new Profile();
        $form = $this->createForm('AppBundle\Form\ProfileType', $profile);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $filePhoto = $profile->getFilePhoto();
            $fileLogo = $profile->getFileLogo();
            if($filePhoto != null){
                $fileNamePhoto = $this->get('app.uploader')->upload($filePhoto);
                $profile->setPhoto($fileNamePhoto);
            }

            if($fileLogo !=null){
                $fileNameLogo = $this->get('app.uploader')->upload($fileLogo);
                $profile->setLogo($fileNameLogo);
            }
            $em = $this->getDoctrine()->getManager();

            $em->persist($profile);
            $em->flush();

            $this->addFlash(
                'success',
                $this->container->get('translator')->trans('main.success')
            );

            return $this->redirectToRoute('profile_show', array('id' => $profile->getId()));
        }

        return $this->render('profile/new.html.twig', array(
            'profile' => $profile,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Profile entity.
     *
     * @Route("/{id}", name="profile_show", requirements={ "id" = "\d+" })
     * @Method("GET")
     */
    public function showAction(Profile $profile)
    {
        $deleteForm = $this->createDeleteForm($profile);
     
        return $this->render('profile/show.html.twig', array(
            'profile' => $profile,
            'delete_form' => $deleteForm->createView(),
        ));
    }
    
    /**
     * Displays a form to edit an existing Profile entity.
     *
     * @Route("/{id}/edit", name="profile_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Profile $profile)
    {
        $deleteForm = $this->createDeleteForm($profile);
        $editForm = $this->createForm('AppBundle\Form\ProfileType', $profile);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $filePhoto = $profile->getFilePhoto();
            $fileLogo = $profile->getFileLogo();
            if($filePhoto != null){
                $this->get('app.uploader')->delete($profile->getPhoto());
                $fileNamePhoto = $this->get('app.uploader')->upload($filePhoto);
                $profile->setPhoto($fileNamePhoto);
            }

            if($fileLogo !=null){
                $this->get('app.uploader')->delete($profile->getLogo());
                $fileNameLogo = $this->get('app.uploader')->upload($fileLogo);
                $profile->setLogo($fileNameLogo);
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($profile);
            $em->flush();

            $this->addFlash(
                'success',
                $this->container->get('translator')->trans('main.success')
            );


            return $this->redirectToRoute('profile_edit', array('id' => $profile->getId()));
        }

        return $this->render('profile/edit.html.twig', array(
            'profile' => $profile,
            'form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Profile entity.
     *
     * @Route("/{id}", name="profile_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Profile $profile)
    {
        $form = $this->createDeleteForm($profile);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($profile);
            $em->flush();
        }

        return $this->redirectToRoute('profile_index');
    }

    /**
     * Creates a form to delete a Profile entity.
     *
     * @param Profile $profile The Profile entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Profile $profile)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('profile_delete', array('id' => $profile->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
