<?php
namespace AppBundle\Controller;

use AppBundle\Entity\Buy;
use AppBundle\Entity\Catalog;
use AppBundle\Entity\Customer;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/account")
 */
class SearchController extends Controller
{
    /**
     * @Route("/search")
     */
    public function searchAction(Request $request)
    {
        $results = array();

        if ($request->get('q')) {
            $q = $request->get('q');

            $objects = $this->get('fos_elastica.finder.app')->find($q, 10);

            foreach ($objects as $object) {
                if ($object instanceof Catalog) {
                    $results[] = array(
                        'href' => $this->get('router')->generate('catalog_show', array('id' => $object->getId())),
                        'label' => (string)$object
                    );
                } else if ($object instanceof Buy) {
                    $results[] = array(
                        'href' => $this->get('router')->generate('buy_show', array('id' => $object->getId())),
                        'label' => (string)$object
                    );
                } else if ($object instanceof Customer) {
                    $results[] = array(
                        'href' => $this->get('router')->generate('customer_show', array('id' => $object->getId())),
                        'label' => (string)$object
                    );
                }
            }
        }

        return $this->render('search/search.html.twig', array(
            'q' => $request->get('q'),
            'results' => $results
        ));
    }
}