<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Invoice;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Invoice controller.
 *
 * @Route("/account/invoice")
 */
class InvoiceController extends Controller
{
    /**
     * Lists all Invoice entities.
     *
     * @Route("/", name="invoice_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $invoices = $em->getRepository('AppBundle:Invoice')->findAll();

        return $this->render('invoice/index.html.twig', array(
            'invoices' => $invoices
        ));
    }

    /**
     * Creates a new Invoice entity.
     *
     * @Route("/new", name="invoice_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $invoice = new Invoice();
        $form = $this->createForm('AppBundle\Form\InvoiceType', $invoice);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($invoice);
            $em->flush();

            $this->addFlash(
                'success',
                $this->container->get('translator')->trans('main.success')
            );

            return $this->redirectToRoute('invoice_show', array('id' => $invoice->getId()));
        }

        $catalog = $this->get('catalog.util')->getCatalog();
        return $this->render('invoice/new.html.twig', array(
            'invoice' => $invoice,
            'form' => $form->createView(),
            'catalog' =>$catalog
        ));
    }

    /**
     * Finds and displays a Invoice entity.
     *
     * @Route("/{id}", name="invoice_show")
     * @Method({"GET", "POST"})
     */
    public function showAction(Request $request, Invoice $invoice)
    {
        $deleteForm = $this->createDeleteForm($invoice);
        $paymentForm = $this->createPaymentForm($invoice);
        $sendForm = $this->createSendForm($invoice);

        $paymentForm = $paymentForm->handleRequest($request);
        $sendForm = $sendForm->handleRequest($request);

        if ($paymentForm->isSubmitted() && $paymentForm->isValid() && $invoice->getRealDatePaiement() === null) {
            $data = $paymentForm->getData();
            $invoice->setRealDatePaiement($data['paymentDate']);
            $invoice->setPaymentMethod($data['paymentMethod']);

            $em = $this->getDoctrine()->getManager();
            $em->persist($invoice);
            $em->flush();
        }

        if ($sendForm->isSubmitted() && $sendForm->isValid()) {
            $data = $sendForm->getData();

            $invoiceContent = $html = $this->renderView(':invoice:print.html.twig', array(
                'base_url' => 'http://nginx',
                'invoice'  => $invoice,
                'base_dir' => $this->get('kernel')->getRootDir() . '/../web' . $request->getBasePath()
            ));
            $invoicePdf = $this->get('knp_snappy.pdf')->getOutputFromHtml($invoiceContent);

            $message = \Swift_Message::newInstance()
                ->setFrom($this->getUser()->getEmail())
                ->setTo($invoice->getCustomer()->getEmail())
                ->setSubject($data['subject'])
                ->setBody($data['message'], 'text/html')
                ->attach(\Swift_Attachment::newInstance()->setFilename('Facture.pdf')->setBody($invoicePdf, 'application/pdf'));
            ;

            $this->get('mailer')->send($message);
        }

        $days = null;
        if(empty($invoice->getRealDatePaiement())){
            $now  = new \DateTime();
            $dead = $invoice->getPaymentDeadline();
            $days =  $now->diff($dead);
        }

        return $this->render('invoice/show.html.twig', array(
            'invoice' => $invoice,
            'days' => $days,
            'delete_form' => $deleteForm->createView(),
            'payment_form' => $paymentForm->createView(),
            'send_form' => $sendForm->createView()
        ));
    }

    /**
     * Finds and displays a Invoice entity.
     *
     * @Route("/{id}/print", name="invoice_print")
     * @Method("GET")
     */
    public function printAction(Request $request, Invoice $invoice){
        
        $html = $this->renderView(':invoice:print.html.twig', array(
            'base_url' => 'http://nginx',
            'invoice'  => $invoice,
            'base_dir' => $this->get('kernel')->getRootDir() . '/../web' . $request->getBasePath()
        ));

        //return new Response($html);
        //exit;
       return new Response(
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html),
            200,
            array(
                'Content-Type'          => 'application/pdf',
                'Content-Disposition'   => 'attachment; filename="Facture'.$invoice->getId().'.pdf"'
            )
        );

    }
    
    /**
     * Displays a form to edit an existing Invoice entity.
     *
     * @Route("/{id}/edit", name="invoice_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Invoice $invoice)
    {
        if ($invoice->getRealDatePaiement() !== null) {
            return $this->redirectToRoute('invoice_index');
        }

        $deleteForm = $this->createDeleteForm($invoice);
        $editForm = $this->createForm('AppBundle\Form\InvoiceType', $invoice);
        $editForm->handleRequest($request);

        $originalLines = $invoice->getLines();
        
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();

            foreach ($invoice->getLines() as $line) {
                if (!$originalLines->contains($line)) {
                    $invoice->removeLine($line);
                }
            }

            $em->persist($invoice);
            $em->flush();

            $this->addFlash(
                'success',
                $this->container->get('translator')->trans('main.success')
            );

            return $this->redirectToRoute('invoice_edit', array('id' => $invoice->getId()));
        }

        $catalog = $this->get('catalog.util')->getCatalog();
        return $this->render('invoice/edit.html.twig', array(
            'invoice' => $invoice,
            'form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'catalog' => $catalog
        ));
    }

    /**
     * Deletes a Invoice entity.
     *
     * @Route("/{id}", name="invoice_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Invoice $invoice)
    {
        $form = $this->createDeleteForm($invoice);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($invoice);
            $em->flush();
        }

        return $this->redirectToRoute('invoice_index');
    }

    /**
     * Creates a form to delete a Invoice entity.
     *
     * @param Invoice $invoice The Invoice entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Invoice $invoice)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('invoice_delete', array('id' => $invoice->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    private function createPaymentForm(Invoice $invoice)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('invoice_show', array('id' => $invoice->getId())))
            ->setMethod('POST')
            ->add('paymentDate', DateType::class, array(
                'label' => 'invoice.show.form.paidThe',
                'widget' => 'single_text',
                'data' => new \DateTime(),
                'constraints' => new \Symfony\Component\Validator\Constraints\Date()
            ))
            ->add('paymentMethod', EntityType::class, array(
                'class' => 'AppBundle\Entity\PaymentMethod',
                'label' => 'invoice.show.form.method',
                'constraints' => new \Symfony\Component\Validator\Constraints\Choice(array('choices' => $this->getDoctrine()->getRepository('AppBundle:PaymentMethod')->findAll()))
            ))
            ->getForm();
    }

    private function createSendForm(Invoice $invoice)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('invoice_show', array('id' => $invoice->getId())))
            ->setMethod('POST')
            ->add('subject', TextType::class, array(
                'label' => 'Subject'
            ))
            ->add('message', TextAreaType::class, array(
                'label' => 'Message'
            ))
            ->getForm();
    }
}
