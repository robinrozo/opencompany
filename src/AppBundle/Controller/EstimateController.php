<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Invoice;
use AppBundle\Entity\InvoiceLine;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Estimate;
use Symfony\Component\HttpFoundation\Response;

/**
 * Estimate controller.
 *
 * @Route("/account/estimate")
 */
class EstimateController extends Controller
{
    private function getCatalogData()
    {
        $catalog = $this->getDoctrine()->getRepository('AppBundle:Catalog')->findAll();

        $items = array();
        foreach ($catalog as $item) {
            $items[] = array(
                'label' => $item->getLabel(),
                'data' => $this->get('serializer')->serialize($item, 'json')
            );
        }

        return $items;
    }

    /**
     * Lists all Estimate entities.
     *
     * @Route("/", name="estimate_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $estimates = $em->getRepository('AppBundle:Estimate')->findAll();

        return $this->render('estimate/index.html.twig', array(
            'estimates' => $estimates,
        ));
    }

    /**
     * Creates a new Estimate entity.
     *
     * @Route("/new", name="estimate_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $estimate = new Estimate();
        $form = $this->createForm('AppBundle\Form\EstimateType', $estimate);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($estimate);
            $em->flush();

            $this->addFlash(
                'success',
                $this->container->get('translator')->trans('main.success')
            );

            return $this->redirectToRoute('estimate_show', array('id' => $estimate->getId()));
        }

        return $this->render('estimate/new.html.twig', array(
            'estimate' => $estimate,
            'form' => $form->createView(),
            'catalog' => $this->getCatalogData()
        ));
    }

    /**
     * Finds and displays a Estimate entity.
     *
     * @Route("/{id}", name="estimate_show")
     * @Method({"GET", "POST"})
     */
    public function showAction(Request $request, Estimate $estimate)
    {
        $deleteForm = $this->createDeleteForm($estimate);

        $sendForm = $this->createSendForm($estimate);
        $sendForm = $sendForm->handleRequest($request);

        if ($sendForm->isSubmitted() && $sendForm->isValid()) {
            $data = $sendForm->getData();

            $invoiceContent = $html = $this->renderView(':estimate:print.html.twig', array(
                'base_url' => 'http://nginx',
                'estimate'  => $estimate
            ));
            $invoicePdf = $this->get('knp_snappy.pdf')->getOutputFromHtml($invoiceContent);

            $message = \Swift_Message::newInstance()
                ->setFrom($this->getUser()->getEmail())
                ->setTo($estimate->getCustomer()->getEmail())
                ->setSubject($data['subject'])
                ->setBody($data['message'], 'text/html')
                ->attach(\Swift_Attachment::newInstance()->setFilename('Devis.pdf')->setBody($invoicePdf, 'application/pdf'));
            ;

            $this->get('mailer')->send($message);
        }

        return $this->render('estimate/show.html.twig', array(
            'estimate' => $estimate,
            'delete_form' => $deleteForm->createView(),
            'send_form' => $sendForm->createView()
        ));
    }

    /**
     * Finds and displays a Estimate entity.
     *
     * @Route("/{id}/print", name="estimate_print")
     * @Method("GET")
     */
    public function printAction(Request $request, Estimate $estimate){

        $html = $this->renderView(':estimate:print.html.twig', array(
            'base_url' => 'http://nginx',
            'estimate'  => $estimate,
            'base_dir' => $this->get('kernel')->getRootDir() . '/../web' . $request->getBasePath()
        ));
        
        return new Response(
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html),
            200,
            array(
                'Content-Type'          => 'application/pdf',

                'Content-Disposition'   => 'attachment; filename="Devis'.$estimate->getId().'.pdf"'
            )
        );

    }

    /**
     * Create an invoice from an estimate
     *
     * @Route("/create_invoice/{id}", name="estimate_create_invoice")
     * @Method("POST")
     */
    public function createInvoiceAction(Request $request, Estimate $estimate)
    {
        $invoice = new Invoice();

        $invoice->setObject('Facture');
        $invoice->setCustomer($estimate->getCustomer());
        $invoice->setDeliveryDate(new \DateTime());
        $invoice->setPaymentDeadline(new \DateTime());
        $invoice->setRealDatePaiement(new \DateTime());
        $invoice->setEstimate($estimate);
        $invoice->setPaymentMethod(null);

        $estimateInvoices = $this->getDoctrine()->getRepository('AppBundle:Invoice')->findBy(array(
            'estimate' => $estimate
        ));

        if ($request->get('type', false)) { // Down payment
            // Compute the total paid
            $totalPaid = 0;
            foreach ($estimateInvoices as $estimateInvoice) {
                $totalPaid += $estimateInvoice->getPriceTotal();
            }
            // Remain to pay
            $totalToPay = $estimate->getTotal() - $totalPaid;

            if ($request->get('type') == 'amount') {
                $typeValue = (float)($request->get('type_value'));

                if ($typeValue > 0) {
                    $totalToPay = $typeValue;
                }
            } else {
                $typeValue = (float)($request->get('type_value'));

                if ($typeValue > 0) {
                    $totalToPay = $totalToPay * ($typeValue / 100);
                }
            }

            $invoiceLine = new InvoiceLine();
            $invoiceLine->setLabel('Accompte');
            $invoiceLine->setPriceUnitary($totalToPay);
            $invoiceLine->setQuantity(1);
            $invoiceLine->setType(null);

            $invoice->addLine($invoiceLine);
        } else { // Total payment
            // Add estimate lines to invoice
            foreach ($estimate->getLines() as $line) {
                $invoiceLine = new InvoiceLine();
                $invoiceLine->setCatalog($line->getCatalog());
                $invoiceLine->setLabel($line->getLabel());
                $invoiceLine->setReference($line->getReference());
                $invoiceLine->setPriceUnitary($line->getPriceUnitary());
                $invoiceLine->setQuantity($line->getQuantity());
                $invoiceLine->setType($line->getType());

                $invoice->addLine($invoiceLine);
            }

            // Substract down payments
            foreach ($estimateInvoices as $estimateInvoice) {
                $invoiceLine = new InvoiceLine();
                $invoiceLine->setLabel('Accompte');
                $invoiceLine->setPriceUnitary(-1 * $estimateInvoice->getPriceTotal());
                $invoiceLine->setQuantity(1);
                $invoiceLine->setType(null);

                $invoice->addLine($invoiceLine);
            }

            $estimate->setIsPaid(true);
        }

        $em = $this->getDoctrine()->getManager();
        $em->persist($estimate);
        $em->persist($invoice);
        $em->flush();

        $this->addFlash(
            'success',
            $this->container->get('translator')->trans('main.success')
        );
        
        $deleteForm = $this->createDeleteForm($estimate);
        $sendForm = $this->createSendForm($estimate);

        return $this->render('estimate/show.html.twig', array(
            'estimate' => $estimate,
            'delete_form' => $deleteForm->createView(),
            'send_form' => $sendForm->createView()
        ));
    }

    /**
     * Displays a form to edit an existing Estimate entity.
     *
     * @Route("/{id}/edit", name="estimate_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Estimate $estimate)
    {
        $deleteForm = $this->createDeleteForm($estimate);
        
        $originalLines = $estimate->getLines();

        $editForm = $this->createForm('AppBundle\Form\EstimateType', $estimate);
        $editForm->handleRequest($request);


        if ($editForm->isSubmitted() && $editForm->isValid()) {
            foreach ($estimate->getLines() as $line) {
                if (!$originalLines->contains($line)) {
                    $estimate->removeLine($line);
                }
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($estimate);
            $em->flush();

            return $this->redirectToRoute('estimate_edit', array('id' => $estimate->getId()));
        }

        return $this->render('estimate/edit.html.twig', array(
            'estimate' => $estimate,
            'form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'catalog' => $this->getCatalogData()
        ));
    }
    

    /**
     * Deletes a Estimate entity.
     *
     * @Route("/{id}", name="estimate_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Estimate $estimate)
    {
        $form = $this->createDeleteForm($estimate);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($estimate);
            $em->flush();
        }

        return $this->redirectToRoute('estimate_index');
    }

    /**
     * Creates a form to delete a Estimate entity.
     *
     * @param Estimate $estimate The Estimate entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Estimate $estimate)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('estimate_delete', array('id' => $estimate->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    private function createSendForm(Estimate $estimate)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('estimate_show', array('id' => $estimate->getId())))
            ->setMethod('POST')
            ->add('subject', TextType::class)
            ->add('message', TextAreaType::class)
            ->getForm();
    }
}
