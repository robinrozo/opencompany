<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\PaymentMethod;
use AppBundle\Form\PaymentMethodType;

/**
 * PaymentMethod controller.
 *
 * @Route("/admin/paymentmethod")
 */
class PaymentMethodController extends Controller
{
    /**
     * Lists all PaymentMethod entities.
     *
     * @Route("/", name="paymentmethod_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $paymentMethods = $em->getRepository('AppBundle:PaymentMethod')->findAll();

        return $this->render('paymentmethod/index.html.twig', array(
            'paymentMethods' => $paymentMethods,
        ));
    }

    /**
     * Creates a new PaymentMethod entity.
     *
     * @Route("/new", name="paymentmethod_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $paymentMethod = new PaymentMethod();
        $form = $this->createForm('AppBundle\Form\PaymentMethodType', $paymentMethod);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($paymentMethod);
            $em->flush();
            
            $this->addFlash(
                'success',
                $this->container->get('translator')->trans('main.success')
            );

            return $this->redirectToRoute('paymentmethod_show', array('id' => $paymentMethod->getId()));
        }

        return $this->render('paymentmethod/new.html.twig', array(
            'paymentMethod' => $paymentMethod,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a PaymentMethod entity.
     *
     * @Route("/{id}", name="paymentmethod_show")
     * @Method("GET")
     */
    public function showAction(PaymentMethod $paymentMethod)
    {
        $deleteForm = $this->createDeleteForm($paymentMethod);

        return $this->render('paymentmethod/show.html.twig', array(
            'paymentMethod' => $paymentMethod,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing PaymentMethod entity.
     *
     * @Route("/{id}/edit", name="paymentmethod_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, PaymentMethod $paymentMethod)
    {
        $deleteForm = $this->createDeleteForm($paymentMethod);
        $editForm = $this->createForm('AppBundle\Form\PaymentMethodType', $paymentMethod);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($paymentMethod);
            $em->flush();

            $this->addFlash(
                'success',
                $this->container->get('translator')->trans('main.success')
            );
            
            return $this->redirectToRoute('paymentmethod_edit', array('id' => $paymentMethod->getId()));
        }

        return $this->render('paymentmethod/edit.html.twig', array(
            'paymentMethod' => $paymentMethod,
            'form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a PaymentMethod entity.
     *
     * @Route("/{id}", name="paymentmethod_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, PaymentMethod $paymentMethod)
    {
        $form = $this->createDeleteForm($paymentMethod);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($paymentMethod);
            $em->flush();
        }

        return $this->redirectToRoute('paymentmethod_index');
    }

    /**
     * Creates a form to delete a PaymentMethod entity.
     *
     * @param PaymentMethod $paymentMethod The PaymentMethod entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(PaymentMethod $paymentMethod)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('paymentmethod_delete', array('id' => $paymentMethod->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
