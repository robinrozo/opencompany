<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;


/**
 * 
 *
 * @Route("/admin/home")
 */
class AdminController extends Controller
{
    /**
     *
     * @Route("/", name="admin_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();


        $em = $this->getDoctrine()->getManager();
        $queryUser = $em->createQuery('SELECT count(u) FROM AppBundle:User u');
        $numUser = $queryUser->getSingleScalarResult();

        $em->getFilters()->disable('user_filter');
        $queryInvoice = $em->createQuery('SELECT count(i) FROM AppBundle:Invoice i');
        $numInvoice =  $queryInvoice->getSingleScalarResult();
  
        $queryEstimate = $em->createQuery('SELECT count(i) FROM AppBundle:Estimate i');
        $numEstimate = $queryEstimate->getSingleScalarResult();

        $queryEstimateRefuse = $em->createQuery('SELECT count(i) FROM AppBundle:Estimate i WHERE i.status = 0');
        $numEstimateRefuse = $queryEstimateRefuse->getSingleScalarResult();

        $queryEstimateAccepte = $em->createQuery('SELECT count(i) FROM AppBundle:Estimate i WHERE i.status = 1');
        $numEstimateAccepte = $queryEstimateAccepte->getSingleScalarResult();

        $queryEstimateEnvoyer = $em->createQuery('SELECT count(i) FROM AppBundle:Estimate i WHERE i.status = 2');
        $numEstimateEnvoyer = $queryEstimateEnvoyer->getSingleScalarResult();

        $firstDayYear = new \DateTime();
        $firstDayYear->modify('first day of January this year');
        $lastDayYear = new \DateTime();
        $lastDayYear->modify('last day of December this year');

        $numInvoiceYear = $em->getRepository('AppBundle:Invoice')->calculCA($firstDayYear, $lastDayYear);

        $firstDayMonth = new \DateTime();
        $firstDayMonth ->modify('first day of this month');
        $lastDayMonth = new \DateTime();
        $lastDayMonth ->modify('last day of this month');

        $numInvoiceMonth= $em->getRepository('AppBundle:Invoice')->calculCA($firstDayMonth, $lastDayMonth);

        $queryInvoiceDelay = $em->createQuery('SELECT count(i) 
                                                FROM AppBundle:Invoice i 
                                                WHERE i.realDatePaiement is null and i.paymentDeadline < :now
                                                ');
        $queryInvoiceDelay->setParameter('now', new \DateTime());
        $numInvoiceDelay = $queryEstimateEnvoyer->getSingleScalarResult();


        $queryPendingPayments = $em->createQuery('SELECT count(i) 
                                                FROM AppBundle:Invoice i 
                                                WHERE i.realDatePaiement is null');
        $numPendingPayments = $queryPendingPayments->getSingleScalarResult();
        $em->getFilters()->enable('user_filter');

        return $this->render(':admin:index.html.twig', array(
            'numUser' =>  $numUser,
            'numInvoice' => $numInvoice,
            'numEstimate' => $numEstimate,
            'numEstimateRefuse' => $numEstimateRefuse,
            'numEstimateAccepte' => $numEstimateAccepte,
            'numEstimateEnvoyer' => $numEstimateEnvoyer,
            'numInvoiceYear' => $numInvoiceYear,
            'numInvoiceMonth' =>  $numInvoiceMonth,
            'numInvoiceDelay' => $numInvoiceDelay,
            'numPendingPayments' => $numPendingPayments,
            'data' => new \DateTime()
        ));
    }

}