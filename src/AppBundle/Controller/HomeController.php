<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\HttpFoundation\Request;


/**
 *
 *
 * @Route("/account/home")
 */
class HomeController extends Controller
{
    /**
     *
     *
     * @Route("/", name="home_index")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        $betweenDateForm = $this->createBetweenDateForm();
        $betweenDateForm = $betweenDateForm->handleRequest($request);

        $result = array_merge(array('between_date_form' => $betweenDateForm->createView()), $this->calcul());

        return $this->render('home/index.html.twig', $result);
    }
    
    /**
     *
     *
     * @Route("/month/{month}", name="home_month")
     * @Method("GET")
     */
    public function monthAction($month, Request $request)
    {

        $firstDayMonth = \DateTime::createFromFormat('d-m-Y', $month)->modify('first day of this month');
        $lastDayMonth = \DateTime::createFromFormat('d-m-Y', $month)->modify('last day of this month');

        $betweenDateForm = $this->createBetweenDateForm($firstDayMonth, $lastDayMonth);
        $betweenDateForm = $betweenDateForm->handleRequest($request);

        $result = array_merge(array('between_date_form' => $betweenDateForm->createView()), $this->calcul($firstDayMonth, $lastDayMonth));

        return $this->render('home/index.html.twig', $result);
    }

    /**
     *
     *
     * @Route("/semester/{date}", name="home_semester")
     * @Method("GET")
     */
    public function semesterAction($date, Request $request)
    {
        $d = \DateTime::createFromFormat('d-m-Y', $date);
        $semester = $this->semester($d);

        $betweenDateForm = $this->createBetweenDateForm($semester['from'], $semester['to']);
        $betweenDateForm = $betweenDateForm->handleRequest($request);

        $result = array_merge(array('between_date_form' => $betweenDateForm->createView()), $this->calcul($semester['from'], $semester['to']));

        return $this->render('home/index.html.twig', $result);

    }

    /**
     *
     *
     * @Route("/year/{year}", name="home_year")
     * @Method("GET")
     */
    public function yearAction($year, Request $request)
    {
        $firstDayYear = \DateTime::createFromFormat('Y', $year)->modify('first day of January this year');
        $lastDayYear = \DateTime::createFromFormat('Y', $year)->modify('last day of December this year');

        $betweenDateForm = $this->createBetweenDateForm($firstDayYear, $lastDayYear);
        $betweenDateForm = $betweenDateForm->handleRequest($request);

        $result = array_merge(array('between_date_form' => $betweenDateForm->createView()), $this->calcul($firstDayYear, $lastDayYear));

        return $this->render('home/index.html.twig', $result);
    }
    
    /**
     *
     *
     * @Route("/betweendate/", name="home_betweendate")
     * @Method({"GET", "POST"})
     */
    public function betweendateAction(Request $request)
    {
        $betweenDateForm = $this->createBetweenDateForm();
        $betweenDateForm = $betweenDateForm->handleRequest($request);
        $dateTo = null;
        $dateFrom = null;

        if ($betweenDateForm->isSubmitted() && $betweenDateForm->isValid() ) {
            $data = $betweenDateForm->getData();
            $dateTo = $data['dateStart'];
            $dateFrom = $data['dateEnd'];
        }

        $result = array_merge(array('between_date_form' => $betweenDateForm->createView()), $this->calcul($dateTo , $dateFrom));
        
        return $this->render('home/index.html.twig', $result);

    }

    private function createBetweenDateForm(\DateTime $dateTo = null, \DateTime $dateFrom = null)
    {
        if ($dateTo === null) {
            $dateTo = new \DateTime('first day of January this year');
        }
        if ($dateFrom === null){
            $dateFrom = new \DateTime();
        }

        return $this->createFormBuilder()
            ->setAction($this->generateUrl('home_betweendate'))
            ->setMethod('POST')
            ->add('dateStart', DateType::class, array(
                'label' => 'Du',
                'data' => $dateTo,
                'widget' => 'single_text',
                'constraints' => new \Symfony\Component\Validator\Constraints\Date()
            ))
            ->add('dateEnd', DateType::class, array(
                'label' => 'Au',
                'data' => $dateFrom,
                'widget' => 'single_text',
                'constraints' => new \Symfony\Component\Validator\Constraints\Date()
            ))
            ->getForm();
    }
    
    private function calcul(\DateTime $dateTo = null, \DateTime $dateFrom = null)
    {
        if ($dateTo === null) {
            $dateTo = new \DateTime('first day of January this year');
        }
        if ($dateFrom === null) {
            $dateFrom = new \DateTime();
        }

        $dateTo->modify('midnight');
        $dateFrom->modify('midnight');

        $em = $this->getDoctrine()->getManager();

        // Chiffre d'affaires
        $CA = $em->getRepository('AppBundle:Invoice')->calculCA($dateTo, $dateFrom);

        //Achats
        $buy = $em->getRepository('AppBundle:Buy')->buyCount($dateTo, $dateFrom);

        //tableau l'impots
        $caType = $em->getRepository('AppBundle:Invoice')->calculCAType($dateTo, $dateFrom);
        $taxTypeRate = $em->getRepository('AppBundle:TaxTypeRate')->findAll();
        $tableauImpots = array();
        $totalTaxes = 0;

        foreach ($taxTypeRate as $type) {
            foreach ($caType as $caItem) {

                if ($type->getId() === $caItem["id"]) {
                    $tableauImpots[] = array(
                        "type" => $type,
                        "ca" => $caItem["prix"],
                        "social" => $caItem["prix"] * $type->getSocial() / 100,
                        "taxes" => $caItem["prix"] * $type->getTaxes() / 100,
                        "totalType" => $caItem["prix"] * $type->getSocial() / 100 + $caItem["prix"] * $type->getTaxes() / 100
                    );
                    $totalTaxes += $caItem["prix"] * $type->getSocial() / 100 + $caItem["prix"] * $type->getTaxes() / 100;
                }
            }
        }

        $benefice = $CA - $buy - $totalTaxes;

        $give = ["ca" => $CA, "buy" => $buy, "be" => $benefice];

        //graph
        $chartCustomer = $this->get('home.util')->dataChartCustomer($dateTo, $dateFrom);
       
        $chartCatalog = $this->get('home.util')->dataChartCatalog($dateTo, $dateFrom);
        
        $chartSupplier = $this->get('home.util')->dataChartSupplier($dateTo, $dateFrom);

        $dataChartArea = $this->get('home.util')->dataChartArea(clone $dateTo, clone $dateFrom);

        $dataComparaisonYear = $this->get('home.util')->dataComparaisonYear(clone $dateTo, clone $dateFrom);

        $now = new \DateTime();
        return array(
            'ca' => $CA,
            'give' => $give,
            'type' => $caType,
            'tableauImpots' => $tableauImpots,
            'data' => $dateFrom,
            'month' => $now,
            'months' => $this->get('home.util')->getMonth(($now->format("m"))),
            'semester' => $this->semester(new \DateTime()),
            'dateStart' => $dateTo,
            'dateEnd' => $dateFrom,
            'totalTaxes' => $totalTaxes,
            'chartCustomer' => $chartCustomer,
            'chartCatalog' => $chartCatalog,
            'chartSupplier' => $chartSupplier,
            'dataChartArea' => $dataChartArea,
            'dataComparaisonYear' => $dataComparaisonYear
        );

    }

    private function semester(\DateTime $date){
        $firstSemesterFrom = \DateTime::createFromFormat('d-m', '01-01');
        $firstSemesterTo =\DateTime::createFromFormat('d-m', '30-06');
        $lastSemesterFrom = \DateTime::createFromFormat('d-m', '01-07');
        $lastSemesterTo =\DateTime::createFromFormat('d-m', '31-12');

        if($date >=$firstSemesterFrom && $date <= $firstSemesterTo){
            return array(
                'from' => $firstSemesterFrom,
                'to' => $firstSemesterTo,
                'label' => '1° Semestre'
            );
        }else{
            return array(
                'from' => $lastSemesterFrom,
                'to' => $lastSemesterTo,
                'label' => '2° Semestre'
            );
        }
    }
}
