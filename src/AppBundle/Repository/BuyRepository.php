<?php
namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class BuyRepository extends EntityRepository
{

    public function buyCount(\DateTime $date1, \DateTime $date2 = null)
    {
        if ($date2 === null) {
            $date2 = new \DateTime();
        }
        
        $em = $this->getEntityManager();
        $query = $em->createQuery('SELECT sum(b.amount) FROM AppBundle:Buy b where b.dateBill >=:date1 and b.dateBill <= :date2 ');
        $query->setParameter('date1', $date1);
        $query->setParameter('date2', $date2);
        try {
            return $query->getSingleScalarResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }

    public function buyMax(\DateTime $date1, \DateTime $date2 =null)
    {
        if ($date2 === null) {
            $date2 = new \DateTime();
        }

        $em = $this->getEntityManager();
        $query = $em->createQuery('SELECT max(b.amount) FROM AppBundle:Buy b where b.dateBill >=:date1 and b.dateBill <= :date2 ');
        $query->setParameter('date1', $date1);
        $query->setParameter('date2', $date2);
        try {
            return $query->getSingleScalarResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }
    
    public function numberSupplier(\DateTime $date1, \DateTime $date2 =null){
        $em = $this->getEntityManager();
        $query = $em->createQuery('SELECT count(distinct b.supplier) FROM AppBundle:Buy b where b.dateBill >=:date1 and b.dateBill <= :date2');
        $query->setParameter('date1', $date1);
        $query->setParameter('date2', $date2);
        try {
            return $query->getSingleScalarResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }

    public  function caBuySupplier(\DateTime $date1, \DateTime $date2)
    {
        $em = $this->getEntityManager();
        $query = $em->createQuery('SELECT s supplier, sum(b.amount) num 
                                    FROM  AppBundle:Supplier s 
                                    JOIN s.buy b 
                                    WHERE b.dateBill >= :date1 and b.dateBill <= :date2
                                    GROUP BY b');

        $query->setParameter('date1', $date1);
        $query->setParameter('date2', $date2);

        try {
            return $query->getResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }
    
    public function caBuyMonth(\DateTime $date1, \DateTime $date2){

        $em = $this->getEntityManager();
        $query = $em->createQuery('SELECT sum(b.amount) num, DATE_FORMAT(b.dateBill, \'%Y-%m\') as year 
                                    FROM AppBundle:Buy b 
                                    WHERE b.dateBill >= :date1 and b.dateBill <= :date2
                                    GROUP BY year');

        $query->setParameter('date1', $date1);
        $query->setParameter('date2', $date2);
        
        try {
            return $query->getResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }
}