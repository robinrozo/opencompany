<?php
namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class InvoiceRepository extends EntityRepository
{

    public function calculCA(\DateTime $date1, \DateTime $date2 = null){

        if ($date2 === null) {
            $date2 = new \DateTime();
        }

        $em = $this->getEntityManager();
        $query = $em->createQuery('SELECT sum(i.priceTotal) FROM AppBundle:Invoice i where i.realDatePaiement >= :date1 and i.realDatePaiement <= :date2');
        $query->setParameter('date1', $date1);
        $query->setParameter('date2', $date2);
        try {
            return $query->getSingleScalarResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }

    public function calculCAMonth(\DateTime $date1, \DateTime $date2){

        $em = $this->getEntityManager();

        $query = $em->createQuery('SELECT sum(i.priceTotal) price, DATE_FORMAT(i.realDatePaiement, \'%Y-%m\')  as year
                                   FROM AppBundle:Invoice i 
                                   WHERE i.realDatePaiement >= :date1 AND i.realDatePaiement <= :date2
                                   GROUP by year');

        $query->setParameter('date1', $date1);
        $query->setParameter('date2', $date2);
       
        try {
            return $query->getResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }

    public function calculCATypeMonth(\DateTime $date1, \DateTime $date2){

        $em = $this->getEntityManager();
        $query = $em->createQuery('SELECT sum(l.quantity * l.priceUnitary) as prix, t.label, t.id, DATE_FORMAT(i.realDatePaiement, \'%Y-%m\')  as year
                                   FROM AppBundle:Invoice i JOIN i.lines l JOIN l.type t
                                   WHERE i.realDatePaiement is not null and i.realDatePaiement >= :date1  and i.realDatePaiement <= :date2
                                   GROUP BY t.id, year');
        $query->setParameter('date1', $date1);
        $query->setParameter('date2', $date2);

        try {
            return $query->getResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }

    public function calculCAType(\DateTime $date1, \DateTime $date2){

        $em = $this->getEntityManager();
        $query = $em->createQuery('SELECT sum(l.quantity * l.priceUnitary) as prix, t.label, t.id
                                   FROM AppBundle:Invoice i JOIN i.lines l JOIN l.type t
                                   WHERE i.realDatePaiement is not null and i.realDatePaiement >= :date1  and i.realDatePaiement <= :date2
                                   GROUP BY t.id');
        $query->setParameter('date1', $date1);
        $query->setParameter('date2', $date2);

        try {
            return $query->getResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }

    /*public function calculTaxes(\DateTime $date1, \DateTime $date2){

        $em = $this->getEntityManager();
        $query = $em->createQuery('SELECT sum(
                                              sum(l.quantity * l.priceUnitary) * t.social/100 +
                                              sum(l.quantity * l.priceUnitary) * t.taxes/100
                                                
                                            )as taxe
                                   FROM AppBundle:Invoice i JOIN i.lines l JOIN l.type t 
                                   WHERE i.realDatePaiement is not null and i.realDatePaiement >= :date1  and i.realDatePaiement <= :date2
                                   GROUP BY t.id');
        $query->setParameter('date1', $date1);
        $query->setParameter('date2', $date2);

        try {
            return $query->getResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }*/

    public function calculCACustomer(\DateTime $date1, \DateTime $date2){
        $em = $this->getEntityManager();

        $query = $em->createQuery('SELECT c customer, SUM(l.quantity * l.priceUnitary) price
                                   FROM AppBundle:Customer c
                                   JOIN c.invoices i JOIN i.lines l
                                   WHERE i.realDatePaiement is not null and i.realDatePaiement >= :date1  and i.realDatePaiement <= :date2
                                   GROUP BY c.id
                                  ');
        $query->setParameter('date1', $date1);
        $query->setParameter('date2', $date2);

        try {
            return $query->getResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }

    public  function calculCACatalog(\DateTime $date1, \DateTime $date2){

        $em = $this->getEntityManager();

        $query = $em->createQuery('SELECT c catalog, SUM(l.quantity * l.priceUnitary) price  
                                   FROM AppBundle:Catalog c 
                                   JOIN c.invoiceLines l
                                   JOIN l.invoice i
                                   WHERE i.realDatePaiement is not null and i.realDatePaiement >= :date1  and i.realDatePaiement <= :date2
                                   GROUP BY c.id');

        $query->setParameter('date1', $date1);
        $query->setParameter('date2', $date2);

        
        try {
            return $query->getResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }


}