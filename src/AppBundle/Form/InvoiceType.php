<?php

namespace AppBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;


class InvoiceType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('object', TextType::class, array(
                'label' => 'invoice.fields.object'
            ))
            ->add('documents', CollectionType::class, array(
                'entry_type' => DocumentType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'required' => false,
                'label' => 'invoice.fields.documents'
            ))
            ->add('deliveryDate', DateType::class, array(
                'label' => 'invoice.fields.deliveryDate',
                'data' => new \DateTime('now'),
                'widget' => 'single_text'
            ))
            ->add('paymentDeadline', DateType::class, array(
                'label' => 'invoice.fields.paymentDeadline',
                'data' => new \DateTime('now'),
                'widget' => 'single_text'
            ))
            ->add('realDatePaiement', DateType::class, array(
                'label' => 'invoice.fields.realDatePaiement',
                'widget' => 'single_text',
                'required' => false
            ))
            ->add('paymentMethod', EntityType::class, array(
                'label' => 'invoice.fields.paymentMethod',
                'class' => 'AppBundle\Entity\PaymentMethod'
            ))
            ->add('customer', EntityType::class, array(
                'label' => 'invoice.fields.customer',
                'class' => 'AppBundle\Entity\Customer'
            ))
            ->add('lines', CollectionType::class, array(
                'entry_type' => InvoiceLineType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false
            ))
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Invoice'
        ));
    }
}
