<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CustomerType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('lastname', TextType::class, array(
                'label' => 'customer.fields.lastname'
            ))
            ->add('firstname', TextType::class, array(
                'label' => 'customer.fields.firstname'
            ))
            ->add('society', TextType::class, array(
                'label' => 'customer.fields.society'
            ))
            ->add('address', TextType::class, array(
                'attr' => array(
                    'class' => 'widget-address'
                ),
                'label' => 'customer.fields.address'
            ))
            ->add('address2',TextType::class,array(
                'label' => 'customer.fields.address2',
                'required' => false
            ))
            ->add('postCode', TextType::class, array(
                'attr' => array(
                    'class' => 'address-postcode'
                ),
                'label' => 'customer.fields.postCode'
            ))
            ->add('city', TextType::class, array(
                'attr' => array(
                    'class' => 'address-city'
                ),
                'label' => 'customer.fields.city'
            ))
            ->add('email', TextType::class, array(
                'label' => 'customer.fields.email'
            ))
            ->add('phone', TextType::class, array(
                'label' => 'customer.fields.phone'
            ))
            ->add('siren', TextType::class, array(
                'label' => 'customer.fields.siren',
                'required' => false
            ))
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Customer'
        ));
    }
}
