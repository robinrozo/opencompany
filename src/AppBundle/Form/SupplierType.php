<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class SupplierType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('lastname', TextType::class, array(
                'label' => 'Nom'
             ))
            ->add('firstname', TextType::class, array(
                 'label' => 'Prénom'
            ))
            ->add('society', TextType::class, array(
                'label' => 'Société'
            ))
            ->add('address', TextareaType::class, array(
                'label' => 'Adresse'
            ))
            ->add('postCode', TextType::class, array(
                'label' => 'Code postal'
            ))
            ->add('city', TextType::class, array(
                'label' => 'Ville'
            ))
            ->add('country', TextType::class, array(
                'label' => 'Pays'
            ))
            ->add('email', TextType::class, array(
                'label' => 'Email'
            ))
            ->add('phone', TextType::class, array(
                'label' => 'Téléphone'
            ))
            ->add('fax', TextType::class, array(
                'label' => 'Fax'
            ))
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Supplier'
        ));
    }
}
