<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class InvoiceLineType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('catalog', EntityType::class,
                array(
                    'class' => 'AppBundle\Entity\Catalog',
                    'placeholder' => 'Custom',
                    'empty_data' => null,
                    'required' => false,
                    'attr' => array(
                        'class' => 'catalog-selector'
                    )
                ))
            ->add('reference')
            ->add('label', null, array("label" => "Designation"))
            ->add('description', TextareaType::class,
                array(
                    'required' => false
                ))
            ->add('quantity', null, array("label" => "Quantité"))
            ->add('priceUnitary', null, array("label" => "prix Unitaire"))
            ->add('type', EntityType::class, array(
                'class' => 'AppBundle\Entity\ActivityType'
            ));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\InvoiceLine'
        ));
    }
}
