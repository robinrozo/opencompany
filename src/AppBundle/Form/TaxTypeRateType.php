<?php

namespace AppBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class TaxTypeRateType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('taxes', NumberType::class, array(
                'label' => 'tax_type_rate.fields.taxes',
                'label_helper' => 'tax_type_rate.helper.message_taxe'
            ))
            ->add('social', NumberType::class, array(
                'label' => 'tax_type_rate.fields.social',
                'label_helper' => 'tax_type_rate.helper.message_social'
            ))
            ->addEventListener(FormEvents::PRE_SET_DATA, array($this, 'onPreSetData'))
        ;
    }

    public function onPreSetData(FormEvent $event)
    {
        $form = $event->getForm();
        $data = $event->getData();

        $form->add('activityType', EntityType::class, array(
            'label' => 'tax_type_rate.fields.activity_type',
            'class' => 'AppBundle\Entity\ActivityType',
            'query_builder' => function(EntityRepository $er) use ($data) {
                $qb = $er->createQueryBuilder('activity_type')
                    ->leftJoin('AppBundle:TaxTypeRate', 'tax_type_rate', 'WITH', 'tax_type_rate.activityType = activity_type.id')
                    ->where('tax_type_rate.id IS NULL');

                    // If activityType is set, keeps it in choice field
                    if ($data->getActivityType()) {
                        $qb->orWhere('tax_type_rate.id = :activity_type_id')
                        ->setParameter('activity_type_id', $data->getActivityType()->getId());
                    }

                return $qb;
            }
        ));
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\TaxTypeRate'
        ));
    }
}
