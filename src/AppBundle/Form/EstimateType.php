<?php

namespace AppBundle\Form;

use AppBundle\Entity\EstimateLine;
use Doctrine\ORM\EntityManager;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EstimateType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('object', TextType::class, array(
                'label' => 'estimate.fields.object'
            ))
            ->add('validUntil', DateType::class, array(
                'label' => 'estimate.fields.validUntil',
                'widget' => 'single_text'
            ))
            ->add('paymentPeriod', NumberType::class, array(
                'label' => 'estimate.fields.paymentPeriod'
            ))
            ->add('latePenalty', NumberType::class, array(
                'label' => 'estimate.fields.latePenalty'
            ))
            ->add('status', ChoiceType::class, array(
                'choices' => array(
                    'Envoyé' => 2,
                    'Accepté' => 1,
                    'Refusé' => 0
                ),
                'label' => 'estimate.fields.status.label'
            ))
            ->add('customer', EntityType::class, array(
                'label' => 'estimate.fields.customer',
                'class' => 'AppBundle\Entity\Customer'
            ))
            ->add('lines', CollectionType::class, array(
                'entry_type' => EstimateLineType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'label' => 'estimate.fields.lines'
            ))
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Estimate'
        ));
    }
}
