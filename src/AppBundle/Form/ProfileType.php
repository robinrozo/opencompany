<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class ProfileType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('lastname', TextType::class, array(
                'label' => 'profile.fields.lastname'
            ))
            ->add('firstname', TextType::class, array(
                'label' => 'profile.fields.firstname'
            ))
            ->add('file_photo', FileType::class, array(
                'label' => 'profile.fields.filePhoto',
                'required' => false,
            ))
            ->add('file_logo', FileType::class, array(
                'label' => 'profile.fields.fileLogo',
                'required' => false,
            ))
            ->add('address1', TextType::class, array(
                'label' => 'profile.fields.address1',
                'attr' => array(
                    'class' => 'widget-address'
                )
            ))
            ->add('address2', TextType::class, array(
                'label' => 'profile.fields.address2'
            ))
            ->add('postCode', TextType::class, array(
                'label' => 'profile.fields.postCode',
                'attr' => array(
                    'class' => 'address-postcode'
                )
            ))
            ->add('city', TextType::class, array(
                'label' => 'profile.fields.city',
                'attr' => array(
                    'class' => 'address-city'
                )
            ))
            ->add('phone', TextType::class, array(
                'label' => 'profile.fields.phone'
            ))
            ->add('siren',TextType::class, array(
                'label' => 'profile.fields.siren'
            ))

        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Profile'
        ));
    }
}
