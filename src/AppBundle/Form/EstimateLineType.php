<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class EstimateLineType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('catalog', EntityType::class,
                array(
                    'class' => 'AppBundle\Entity\Catalog',
                    'empty_data' => null,
                    'placeholder' => 'Custom',
                    'required' => false,
                    'label' => 'estimate.fields.catalog',
                    'attr' => array(
                        'class' => 'catalog-selector'
                    )
                ))
            ->add('label', TextType::class,
                array(
                    'label' => 'estimate.fields.label'
                ))
            ->add('description', TextareaType::class,
                array(
                    'required' => false,
                    'label' => 'estimate.fields.description'
                )
            )
            ->add('reference', TextType::class,
                array(
                    'label' => 'estimate.fields.reference'
                ))
            ->add('priceUnitary', NumberType::class,
                array(
                    'label' => 'estimate.fields.priceUnitary'
                ))
            ->add('quantity', NumberType::class,
                array(
                    'label' => 'estimate.fields.quantity'
                ))
            ->add('type', EntityType::class, array(
                'class' => 'AppBundle\Entity\ActivityType',
                'label' => 'estimate.fields.type'
            ));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\EstimateLine'
        ));
    }

    public function getName()
    {
        return 'estimateline';
    }
}
