$(document).ready(function() {
    $('.widget-daterangepicker').daterangepicker({
        singleDatePicker: true,
        calender_style: "picker_1",
        format: 'YYYY-MM-DD'
    });

    $('.widget-address').each(function(index, elem) {
        places({
            container: elem
        }).on('change', function(e) {
            var $address = $('.widget-address');
            var $postcode = $('.address-postcode');
            var $city = $('.address-city');

            $address.val(e.suggestion.name);
            $postcode.val(e.suggestion.hit.postcode[0]);
            $city.val(e.suggestion.hit.city[0]);
        });
    });
    
    $('#datatable').DataTable();
});

