<?php
namespace AppBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class Builder implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    public function mainMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root', array(
            'childrenAttributes' => array(
                'class' => 'nav side-menu'
            )
        ));

        $menu->addChild('Accueil', array('route' => 'home_index'))
            ->setLabelAttribute('icon', 'fa-home');

        $menu->addChild('Achats', array('route' => 'buy_index'))
            ->setLabelAttribute('icon', 'fa-shopping-cart');

        $menu->addChild('Catalogue', array('route' => 'catalog_index'))
            ->setLabelAttribute('icon', 'fa-book');

        $menu->addChild('Clients', array('route' => 'customer_index'))
            ->setLabelAttribute('icon', 'fa-user');

        $menu->addChild('Fournisseurs', array('route' => 'supplier_index'))
            ->setLabelAttribute('icon', 'fa-truck');

        $menu->addChild('Mes Factures', array('route' => 'invoice_index'))
            ->setLabelAttribute('icon', 'fa-eur');

        $menu->addChild('Mes Devis', array('route' => 'estimate_index'))
            ->setLabelAttribute('icon', 'fa-eur');

        if ($this->container->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            $menu->addChild('Utilisateurs', array('route' => 'user_index'))
                ->setLabelAttribute('icon', 'fa-users');

            $menu->addChild('Type de taxes', array('route' => 'taxtyperate_index'))
                ->setLabelAttribute('icon', 'fa-institution');

            $menu->addChild('Type d\'activité', array('route' => 'activitytype_index'))
                ->setLabelAttribute('icon', 'fa-gavel');

            $menu->addChild('Méthode de paiement', array('route' => 'paymentmethod_index'))
                ->setLabelAttribute('icon', 'fa-credit-card');

            $menu->addChild('Supplémentaire', array('route' => 'admin_index'))
                ->setLabelAttribute('icon', 'fa-star');
        }
        
        return $menu;
    }
}