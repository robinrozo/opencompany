<?php
namespace AppBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;

class HomeUtil
{
    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function dataChartCatalog(\DateTime $dateTo, \DateTime $dateFrom)
    {

        $caCatalog = $this->container->get('doctrine')->getRepository('AppBundle:Invoice')->calculCACatalog($dateTo, $dateFrom);

        $legend = array();
        $data = array();
        $color = array();

        foreach ($caCatalog as $caCat) {
            $legend[] = $caCat['catalog']->getLabel();
            $data[] = $caCat['price'];
            $color[] = $this->rand_color();
        }

        $datasets[] = ["data" => $data, "backgroundColor" => $color, "label" => ""];
        $chartData = ["datasets" => $datasets, "labels" => $legend];

        return $this->container->get('serializer')->serialize($chartData, 'json');
    }

    public function dataChartCustomer(\DateTime $dateTo, \DateTime $dateFrom)
    {

        $caCustomer = $this->container->get('doctrine')->getRepository('AppBundle:Invoice')->calculCACustomer($dateTo, $dateFrom);
        
        $data = array();
        $color = array();

        foreach ($caCustomer as $caCus) {
           
            $color[] = $this->rand_color();
            $data[] = array(
                'name' => $caCus['customer']->getLastname() . " " . $caCus['customer']->getFirstname(),
                'value' => $caCus['price']
            );
        }

        $datasets = ["data" =>$this->container->get('serializer')->serialize($data, 'json'),
                     "backgroundColor" => $this->container->get('serializer')->serialize($color,'json')];
        
        return $datasets;
    }

    public function dataChartSupplier(\DateTime $date1, \DateTime $date2)
    {

        $buySupplier = $this->container->get('doctrine')->getRepository('AppBundle:Buy')->caBuySupplier($date1, $date2);

        $legend = array();
        $data = array();
        $color = array();

        foreach ($buySupplier as $buy) {
            $legend[] = (string)$buy['supplier'];
            $data[] = $buy['num'];
            $color[] = $this->rand_color();
        }

        $datasets[] = ["data" => $data, "backgroundColor" => $color, "label" => ""];
        $chartData = ["datasets" => $datasets, "labels" => $legend];

        return $this->container->get('serializer')->serialize($chartData, 'json');
    }

    public function dataChartArea(\DateTime $date1, \DateTime $date2)
    {

        $caBuyMonth = $this->container->get('doctrine')->getRepository('AppBundle:Buy')->caBuyMonth($date1, $date2);
        $CA = $this->container->get('doctrine')->getRepository('AppBundle:Invoice')->calculCAMonth($date1, $date2);
        $caType = $this->container->get('doctrine')->getRepository('AppBundle:Invoice')->calculCATypeMonth($date1, $date2);
        $taxTypeRate = $this->container->get('doctrine')->getRepository('AppBundle:TaxTypeRate')->findAll();
        
        $tableauImpots = array();
        foreach ($taxTypeRate as $type) {
            foreach ($caType as $caItem) {

                if ($type->getId() === $caItem["id"]) {
                    $tableauImpots[$caItem['year']] = $caItem["prix"] * $type->getSocial() / 100 + $caItem["prix"] * $type->getTaxes() / 100;
                }
            }
        }

        $current = $date1;
        $data = array();

        while ($current < $date2) {

            $month_current = $current->format("Y-m");
            $ca_month_current = 0;
            $be_month_current = 0;
            $buy_month_current = 0;

            if (!isset($tableauImpots[$month_current])) {
                $tableauImpots[$month_current] = 0;
            }

            foreach ($CA as $itemCa) {
                if ($itemCa['year'] === $month_current) {
                    $ca_month_current = $itemCa['price'];
                }
            }

            foreach ($caBuyMonth as $itemBuy) {
                if ($itemBuy['year'] === $month_current) {
                    $buy_month_current = $itemBuy['num'];
                }
            }

            $be_month_current = $ca_month_current - $buy_month_current - $tableauImpots[$month_current];
            $data[] = ["period" => $current->format("Y-m"),
                "be" => $be_month_current,
                "ca" => $ca_month_current,
                "buy" => $buy_month_current
                ];
            $current = $current->modify('+1 month');
        }
      
        return $this->container->get('serializer')->serialize($data, 'json');

    }

    public function dataComparaisonYear(\DateTime $date1, \DateTime $date2){

        $CA_current = $this->container->get('doctrine')->getRepository('AppBundle:Invoice')->calculCAMonth($date1, $date2);
        $buy_current = $this->container->get('doctrine')->getRepository('AppBundle:Buy')->caBuyMonth($date1, $date2);
        
        $dateTo = clone $date1;
        $dateFrom = clone $date2;
        $yearNow = $dateTo->format('Y');
        $dateToBefore = $date1->modify('-1 year');
        $dataFromBefore = $date2->modify('-1 year');
        $yearLast = $dateToBefore->format('Y');

        $CA_before = $this->container->get('doctrine')->getRepository('AppBundle:Invoice')->calculCAMonth($dateToBefore, $dataFromBefore);
        $buy_before = $this->container->get('doctrine')->getRepository('AppBundle:Buy')->caBuyMonth($dateToBefore, $dataFromBefore);

        $label = array();

        $current = $dateTo;
        $data = array();
        $dataBuy = array();

        while ($current < $dateFrom) {
            $label[] = $this->getMonth($current->format("m"));
            $month_current = $current->format("Y-m");
            $dCA = 0;
            $dBuy = 0;
            foreach ($CA_current AS $ca){
               if($ca['year'] === $month_current){
                   $dCA = $ca['price'];
               }
            }
            foreach ($buy_current AS $buy){
                if($buy['year'] === $month_current){
                    $dBuy = $buy['num'];
                }
            }
            $data[] = $dCA;
            $dataBuy[] = $dBuy;

            $current = $current->modify('+1 month');
        }

        $current1 = $dateToBefore;
        $data2 = array();
        $dataBuy2 = array();
        while ($current1 < $dataFromBefore) {
            $month_current1 = $current1->format("Y-m");
            $d = 0;
            $dBuy = 0;
            foreach ($CA_before as $item) {
                if($item['year'] === $month_current1){
                    $d = $item['price'];
                }
            }
            foreach ($buy_current AS $buy){
                if($buy['year'] === $month_current){
                    $dBuy = $buy['num'];
                }
            }
            $data2[] = $d;
            $dataBuy2[] = $dBuy;

            $current1 = $current1->modify('+1 month');
        }

        $datasetsCA0 = ['label' => 'Chiffre d\'affaire ' . $yearNow,
            'backgroundColor' => "#26B99A",
            'data' => $data];
        $datasetsBuy0 = ['label' => 'Achats ' . $yearNow,
            'backgroundColor' => "#9B59B6",
            'data' => $dataBuy];
        $datasetsCA1 = ['label' => 'Chiffre d\'affaire ' . $yearLast,
            'backgroundColor' => "#03586A",
            'data' => $data2];
        $datasetsBuy1 = ['label' => 'Achats ' . $yearLast,
            'backgroundColor' => "#BDC3C7",
            'data' => $dataBuy2];

        $datasets = [$datasetsCA0, $datasetsCA1, $datasetsBuy0, $datasetsBuy1];
        $chartData = ['labels' => $label, 'datasets' => $datasets];
   
        return $this->container->get('serializer')->serialize($chartData, 'json');


    }

    public function getMonth($month)
    {
        $months = array('01' => $this->container->get('translator')->trans('calendar.january'),
            '02' => $this->container->get('translator')->trans('calendar.february'),
            '03' => $this->container->get('translator')->trans('calendar.march'),
            '04' => $this->container->get('translator')->trans('calendar.april'),
            '05' => $this->container->get('translator')->trans('calendar.may'),
            '06' => $this->container->get('translator')->trans('calendar.june'),
            '07' => $this->container->get('translator')->trans('calendar.july'),
            '08' => $this->container->get('translator')->trans('calendar.august'),
            '09' => $this->container->get('translator')->trans('calendar.september'),
            '10' => $this->container->get('translator')->trans('calendar.october'),
            '11' => $this->container->get('translator')->trans('calendar.november'),
            '12' => $this->container->get('translator')->trans('calendar.december'));
        return $months[$month];
    }

    private function rand_color()
    {

        return sprintf('#%06X', mt_rand(0, 0xFFFFFF));
    }

}