<?php
namespace AppBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;

class CatalogUtil
{
    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function getCatalog()
    {
        $catalog = $this->container->get('doctrine')->getRepository('AppBundle:Catalog')->findAll();

        $items = array();
        foreach ($catalog as $item) {
            $items[] = array(
                'label' => $item->getLabel(),
                'data' => $this->container->get('serializer')->serialize($item, 'json')
            );
        }

        return $items;
    }
}