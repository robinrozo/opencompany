<?php
namespace AppBundle\Filter;

use Doctrine\Common\Annotations\Reader;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Query\Filter\SQLFilter;

class UserFilter extends SQLFilter
{
    protected $reader;

    /**
     * @var $em ObjectManager
     */
    protected $em;

    /**
     * Gets the SQL query part to add to a query.
     *
     * @param ClassMetaData $targetEntity
     * @param string $targetTableAlias
     *
     * @return string The constraint SQL if there is available, empty string otherwise.
     */
    public function addFilterConstraint(ClassMetadata $targetEntity, $targetTableAlias)
    {
        if (empty($this->reader)) {
            return '';
        }

        // Check if entity is "user aware"
        $userAware = $this->reader->getClassAnnotation(
            $targetEntity->getReflectionClass(),
            'AppBundle\Annotation\UserAware'
        );

        if (!$userAware) {
            return '';
        }

        $fieldName = $userAware->property;

        try {
            $userId = $this->getParameter('id');
        } catch (\InvalidArgumentException $e) {
            return '';
        }

        if (empty($fieldName) || empty($userId)) {
            return '';
        }

        $columnName = $targetEntity->getAssociationMapping($fieldName)['joinColumns'][0]['name'];

        $query = sprintf('%s.%s = %s', $targetTableAlias, $columnName, $userId);

        return $query;
    }

    public function setAnnotationReader(Reader $reader)
    {
        $this->reader = $reader;
    }
}