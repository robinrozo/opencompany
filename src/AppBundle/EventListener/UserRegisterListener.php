<?php
namespace AppBundle\EventListener;

use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class UserRegisterListener
{
    protected $tokenStorage;

    protected $router;

    public function __construct(TokenStorage $tokenStorage, Router $router)
    {
        $this->tokenStorage = $tokenStorage;
        $this->router = $router;
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        if (!$event->isMasterRequest()) {
            return;
        }

        if ($this->tokenStorage->getToken() !== null) {
            $user = $this->tokenStorage->getToken()->getUser();

            if ($user instanceof User && $user->getProfile() === null) {
                // Avoid looping
                $route = $this->router->matchRequest($event->getRequest());
                if ($route && $route['_route'] == 'profile_new') {
                    return;
                }

                $url = $this->router->generate('profile_new');

                $event->setResponse(new RedirectResponse($url));
                return;
            }
        }
    }
}