<?php
namespace AppBundle\EventListener;

use Doctrine\Common\Annotations\Reader;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class UserAwareListener
{
    protected $tokenStorage;

    protected $reader;

    public function __construct(TokenStorageInterface $tokenStorage, Reader $reader)
    {
        $this->tokenStorage = $tokenStorage;

        $this->reader = $reader;
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $user = $this->getUser();

        if ($user === null) {
            return;
        }

        $entity = $args->getEntity();

        $userAware = $this->reader->getClassAnnotation(new \ReflectionClass($entity), 'AppBundle\Annotation\UserAware');

        if ($userAware) {
            $property = $userAware->property;
            $method = 'set'.ucfirst($property);

            $entity->$method($user);
        }
    }

    private function getUser()
    {
        $token = $this->tokenStorage->getToken();

        if (!$token) {
            return null;
        }

        $user = $token->getUser();

        if (!($user instanceof UserInterface)) {
            return null;
        }

        return $user;
    }
}