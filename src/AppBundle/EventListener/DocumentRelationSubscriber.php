<?php
namespace AppBundle\EventListener;

use AppBundle\Entity\Document;
use AppBundle\Entity\Invoice;
use Doctrine\Common\EventSubscriber;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\LoadClassMetadataEventArgs;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Events;
use Doctrine\ORM\PersistentCollection;
use Symfony\Component\DependencyInjection\ContainerInterface;

class DocumentRelationSubscriber implements EventSubscriber
{
    const INTERFACE_NAMESPACE = 'AppBundle\Entity\Behavior\HasDocumentInterface';

    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * Returns an array of events this subscriber wants to listen to.
     *
     * @return array
     */
    public function getSubscribedEvents()
    {
        return array(
            Events::loadClassMetadata,
            Events::prePersist,
            Events::preUpdate,
            Events::onFlush
        );
    }

    /**
     * @param LoadClassMetadataEventArgs $eventArgs
     */
    public function loadClassMetadata(LoadClassMetadataEventArgs $eventArgs)
    {
        // the $metadata is the whole mapping info for this class
        $metadata = $eventArgs->getClassMetadata();

        if (!in_array(self::INTERFACE_NAMESPACE, class_implements($metadata->getName()))) {
            return;
        }

        $namingStrategy = $eventArgs
            ->getEntityManager()
            ->getConfiguration()
            ->getNamingStrategy();

        $metadata->mapManyToMany(array(
            'targetEntity' => Document::CLASS,
            'fieldName' => 'documents',
            'cascade' => array('all'),
            'orphanRemoval' => true,
            'joinTable' => array(
                'name' => strtolower($namingStrategy->classToTableName($metadata->getName())) . '_document',
                'joinColumns' => array(
                    array(
                        'name' => $namingStrategy->joinKeyColumnName($metadata->getName()),
                        'referencedColumnName' => $namingStrategy->referenceColumnName(),
                        'onDelete' => 'CASCADE',
                        'onUpdate' => 'CASCADE',
                    ),
                ),
                'inverseJoinColumns' => array(
                    array(
                        'name' => 'document_id',
                        'referencedColumnName' => $namingStrategy->referenceColumnName(),
                        'onDelete' => 'CASCADE',
                        'onUpdate' => 'CASCADE',
                    ),
                )
            )
        ));
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        if ($entity instanceof Document) {
            $this->setDocumentData($entity);
        }
    }

    public function preUpdate(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        if ($entity instanceof Document) {
            $this->setDocumentData($entity);
        }
    }

    protected function setDocumentData(Document $document)
    {
        if ($document->getName() === null) {
            $document->setName(md5(uniqid()).'.'.$document->getFile()->getExtension());
        }

        $document->setMimeType($document->getFile()->getMimeType());
    }

    public function onFlush(OnFlushEventArgs $eventArgs)
    {
        $em = $eventArgs->getEntityManager();
        $uow = $em->getUnitOfWork();

        foreach ($uow->getScheduledEntityInsertions() as $entity) {
            if ($entity instanceof Document) {
                $this->uploadDocument($entity);
            }
        }

        foreach ($uow->getScheduledEntityDeletions() as $entity) {
            if ($entity instanceof Document) {
                if (file_exists($this->container->getParameter('document_directory').$entity->getName())) {
                    unlink($this->container->getParameter('document_directory').$entity->getName());
                }
            }
        }
    }

    protected function uploadDocument(Document $document)
    {
        $file = $document->getFile();
        $documentDirectory = $this->container->getParameter('document_directory');

        if ($file) {
            $file->move($documentDirectory, $document->getName());
        }
    }
}