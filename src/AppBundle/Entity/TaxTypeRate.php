<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 */
class TaxTypeRate{
    /**
     * @ORM\Column(type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;


    /**
     * @ORM\Column(type="float")
     *
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @Assert\Type(type="float")
     */
    protected $taxes;

    /**
     * @ORM\Column(type="float")
     *
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @Assert\Type(type="float")
     */
    protected $social;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\ActivityType", inversedBy="taxTypeRate")
     *
     * @Assert\NotNull()
     */
    protected $activityType;
    
    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }
    

    /**
     * @return mixed
     */
    public function getTaxes()
    {
        return $this->taxes;
    }

    /**
     * @param mixed $taxes
     */
    public function setTaxes($taxes)
    {
        $this->taxes = $taxes;
    }

    /**
     * @return mixed
     */
    public function getSocial()
    {
        return $this->social;
    }

    /**
     * @param mixed $social
     */
    public function setSocial($social)
    {
        $this->social = $social;
    }

    /**
     * @return mixed
     */
    public function getActivityType()
    {
        return $this->activityType;
    }

    /**
     * @param mixed $activityType
     */
    public function setActivityType($activityType)
    {
        $this->activityType = $activityType;
    }
    
}