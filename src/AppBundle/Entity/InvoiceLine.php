<?php
namespace AppBundle\Entity;

use AppBundle\Annotation\UserAware;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 *
 * @UserAware(property="user")
 */
class InvoiceLine
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="bigint")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @Assert\Length(max=255)
     */
    protected $label;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $reference;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $description;

    /**
     * @ORM\Column(type="integer")
     *
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @Assert\Type(type="integer")
     * @Assert\GreaterThan(value=0)
     */
    protected $quantity;

    /**
     * @ORM\Column(type="decimal")
     *
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @Assert\Type(type="float")
     */
    protected $priceUnitary;

    /**
     * @ORM\ManyToOne(targetEntity="ActivityType")
     *
     * @Assert\NotNull()
     */
    protected $type;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Invoice", inversedBy="lines")
     */
    protected $invoice;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Catalog", inversedBy="invoiceLines")
     */
    protected $catalog;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     */
    protected $user;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param mixed $label
     */
    public function setLabel($label)
    {
        $this->label = $label;
    }

    /**
     * @return mixed
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param mixed $quantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }

    /**
     * @return mixed
     */
    public function getPriceUnitary()
    {
        return $this->priceUnitary;
    }

    /**
     * @param mixed $priceUnitary
     */
    public function setPriceUnitary($priceUnitary)
    {
        $this->priceUnitary = $priceUnitary;
    }
    

    /**
     * @return mixed
     */
    public function getInvoice()
    {
        return $this->invoice;
    }

    /**
     * @param mixed $invoice
     */
    public function setInvoice($invoice)
    {
        $this->invoice = $invoice;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * @param mixed $reference
     */
    public function setReference($reference)
    {
        $this->reference = $reference;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     * @return InvoiceLine
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCatalog()
    {
        return $this->catalog;
    }

    /**
     * @param mixed $catalog
     * @return InvoiceLine
     */
    public function setCatalog($catalog)
    {
        $this->catalog = $catalog;
        return $this;
    }

}