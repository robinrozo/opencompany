<?php
namespace AppBundle\Entity\Behavior;

use AppBundle\Entity\Document;
use Doctrine\Common\Collections\ArrayCollection;

interface HasDocumentInterface
{
    public function addDocument(Document $uploadedDocument);

    public function removeDocument(Document $uploadedDocument);

    public function getDocuments();

    public function setDocuments(ArrayCollection $documents);
}