<?php
namespace AppBundle\Entity\Behavior;

use AppBundle\Entity\Document;
use Doctrine\Common\Collections\ArrayCollection;

trait HasDocumentTrait
{
    /**
     * @var ArrayCollection
     */
    protected $documents;

    public function addDocument(Document $uploadedDocument)
    {
        $this->documents->add($uploadedDocument);
        return $this;
    }

    public function removeDocument(Document $uploadedDocument)
    {
        $this->documents->removeElement($uploadedDocument);
    }

    public function getDocuments()
    {
        return $this->documents;
    }

    public function setDocuments(ArrayCollection $documents)
    {
        $this->documents = $documents;
        return $this;
    }
}