<?php


namespace AppBundle\Entity;

use AppBundle\Annotation\UserAware;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BuyRepository")
 * @JMS\ExclusionPolicy("all")
 * @UserAware(property="user")
 */
class Buy
{
    /**
     * @ORM\Column(type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     *
     * @JMS\Expose()
     * @JMS\Groups({"elastica"})
     */
    protected $id;

    /**
     * @ORM\Column(type="string",length=255)
     *
     * @Assert\Length(max=255)
     *
     * @JMS\Expose()
     * @JMS\Groups({"elastica"})
     */
    protected $referenceBill;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Assert\NotNull
     * @Assert\NotBlank
     * @Assert\Length(max=255)
     *
     * @JMS\Expose()
     * @JMS\Groups({"elastica"})
     */
    protected $label;

    /**
     * @ORM\Column(type="date")
     * @Assert\Date()
     */
    protected $dateBill;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     *
     * @JMS\Expose()
     * @JMS\Groups({"elastica"})
     */
    protected $user;

    /**
     * @ORM\Column(type="decimal")
     *
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @Assert\Type(type="float")
     * @Assert\GreaterThan(value=0)
     */
    protected $amount;

    /**
     * @ORM\ManyToOne(targetEntity="Supplier")
     *
     * @Assert\NotNull()
     */
    protected $supplier;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getReferenceBill()
    {
        return $this->referenceBill;
    }

    /**
     * @param mixed $referenceBill
     */
    public function setReferenceBill($referenceBill)
    {
        $this->referenceBill = $referenceBill;
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param mixed $label
     */
    public function setLabel($label)
    {
        $this->label = $label;
    }
    
    /**
     * @return mixed
     */
    public function getDateBill()
    {
        return $this->dateBill;
    }

    /**
     * @param mixed $dateBill
     */
    public function setDateBill($dateBill)
    {
        $this->dateBill = $dateBill;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param mixed $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return mixed
     */
    public function getSupplier()
    {
        return $this->supplier;
    }

    /**
     * @param mixed $supplier
     */
    public function setSupplier($supplier)
    {
        $this->supplier = $supplier;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     * @return Buy
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    public function __toString()
    {
        return 'Achat ' . $this->getSupplier() . ' ' . $this->getAmount() . ' ' . $this->getDateBill()->format('d-m-Y');
    }
}