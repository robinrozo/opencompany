<?php
namespace AppBundle\Entity;

use AppBundle\Annotation\UserAware;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @JMS\ExclusionPolicy("all")
 * @UserAware(property="user")
 */
class Catalog
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="bigint")
     *
     * @JMS\Expose()
     * @JMS\Groups({"elastica"})
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @Assert\Length(max=255)
     *
     * @JMS\Expose()
     * @JMS\Groups({"elastica"})
     */
    protected $label;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $reference;

    /**
     * @ORM\Column(type="float")
     *
     * @Assert\NotNull()
     * @Assert\Type(type="float")
     */
    protected $unitPriceTaxExcluded;

    /**
     * @ORM\ManyToOne(targetEntity="ActivityType")
     *
     * @Assert\NotNull()
     */
    protected $type;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     *
     * @JMS\Expose()
     * @JMS\Groups({"elastica"})
     */
    protected $user;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\EstimateLine", mappedBy="catalog")
     */
    protected $estimateLines;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\InvoiceLine", mappedBy="catalog")
     */
    protected $invoiceLines;
    
    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Catalog
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param mixed $label
     * @return Catalog
     */
    public function setLabel($label)
    {
        $this->label = $label;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * @param mixed $reference
     * @return Catalog
     */
    public function setReference($reference)
    {
        $this->reference = $reference;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUnitPriceTaxExcluded()
    {
        return $this->unitPriceTaxExcluded;
    }

    /**
     * @param mixed $unitPriceTaxExcluded
     * @return Catalog
     */
    public function setUnitPriceTaxExcluded($unitPriceTaxExcluded)
    {
        $this->unitPriceTaxExcluded = $unitPriceTaxExcluded;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     * @return Catalog
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     * @return Catalog
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEstimateLines()
    {
        return $this->estimateLines;
    }

    /**
     * @param mixed $estimateLines
     * @return Catalog
     */
    public function setEstimateLines($estimateLines)
    {
        $this->estimateLines = $estimateLines;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getInvoiceLines()
    {
        return $this->invoiceLines;
    }

    /**
     * @param mixed $invoiceLines
     * @return Catalog
     */
    public function setInvoiceLines($invoiceLines)
    {
        $this->invoiceLines = $invoiceLines;
        return $this;
    }

    public function __toString()
    {
        return $this->getLabel();
    }
}