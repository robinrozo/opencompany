<?php
namespace AppBundle\Entity;

use AppBundle\Annotation\UserAware;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity
 * @JMS\ExclusionPolicy("all")
 * @UserAware(property="user")
 */
class Customer
{
    /**
     * @ORM\Column(type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     *
     * @JMS\Expose()
     * @JMS\Groups({"elastica"})
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @Assert\Length(max=255)
     *
     * @JMS\Expose()
     * @JMS\Groups({"elastica"})
     */
    protected $lastname;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @Assert\Length(max=255)
     *
     * @JMS\Expose()
     * @JMS\Groups({"elastica"})
     */
    protected $firstname;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @Assert\Length(max=255)
     *
     * @JMS\Expose()
     * @JMS\Groups({"elastica"})
     */
    protected $society;

    /**
     * @ORM\Column(type="string")
     *
     * @Assert\NotNull()
     * @Assert\NotBlank()
     *
     * @JMS\Expose()
     * @JMS\Groups({"elastica"})
     */
    protected $address;

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     * @JMS\Expose()
     * @JMS\Groups({"elastica"})
     */
    protected $address2;

    /**
     * @ORM\Column(type="string", length=5)
     *
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @Assert\Length(max=5, min=5)
     * @Assert\Regex("/[0-9]{2}[0-9]{3}/", message="validator.invalid_postcode")
     *
     * @JMS\Expose()
     * @JMS\Groups({"elastica"})
     */
    protected $postCode;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @Assert\Length(max=255)
     *
     * @JMS\Expose()
     * @JMS\Groups({"elastica"})
     */
    protected $city;

    /**
     * @ORM\Column(type="string")
     *
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @Assert\Email()
     *
     * @JMS\Expose()
     * @JMS\Groups({"elastica"})
     */
    protected $email;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @Assert\Regex("/\d{10}/", message="validator.invalid_phone")
     *
     * @JMS\Expose()
     * @JMS\Groups({"elastica"})
     */
    protected $phone;

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     * @Assert\Luhn(message = "user.siret.error")
     */
    protected $siren;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     *
     * @JMS\Expose()
     * @JMS\Groups({"elastica"})
     */
    protected $user;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Invoice", mappedBy="customer")
     */
    protected $invoices;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Estimate", mappedBy="customer")
     */
    protected $estimate;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Customer
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @param mixed $lastname
     * @return Customer
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * @param mixed $firstname
     * @return Customer
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSociety()
    {
        return $this->society;
    }

    /**
     * @param mixed $society
     * @return Customer
     */
    public function setSociety($society)
    {
        $this->society = $society;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     * @return Customer
     */
    public function setAddress($address)
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAddress2()
    {
        return $this->address2;
    }

    /**
     * @param mixed $address2
     * @return Customer
     */
    public function setAddress2($address2)
    {
        $this->address2 = $address2;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPostCode()
    {
        return $this->postCode;
    }

    /**
     * @param mixed $postCode
     * @return Customer
     */
    public function setPostCode($postCode)
    {
        $this->postCode = $postCode;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     * @return Customer
     */
    public function setCity($city)
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     * @return Customer
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return mixed
     */
    public function getSiren()
    {
        return $this->siren;
    }

    /**
     * @param mixed $siren
     */
    public function setSiren($siren)
    {
        $this->siren = $siren;
    }
    
    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     * @return Customer
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getInvoices()
    {
        return $this->invoices;
    }

    /**
     * @param mixed $invoices
     * @return Customer
     */
    public function setInvoices($invoices)
    {
        $this->invoices = $invoices;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEstimate()
    {
        return $this->estimate;
    }

    /**
     * @param mixed $estimate
     * @return Customer
     */
    public function setEstimate($estimate)
    {
        $this->estimate = $estimate;
        return $this;
    }
    
    public function __toString()
    {
        return $this->getFirstname() . ' ' . $this->getLastname();
    }
    
}