<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 */
 class Document
 {

     /**
      * @ORM\Id
      * @ORM\GeneratedValue(strategy="IDENTITY")
      * @ORM\Column(type="bigint")
      */
     protected $id;

     /**
      * @ORM\Column(type="string")
      */
     protected $name;

     /**
      * @ORM\Column(type="string")
      */
     protected $mimeType;

     /**
      * @Assert\File()
      */
     protected $file;

     /**
      * @return mixed
      */
     public function getId()
     {
         return $this->id;
     }

     /**
      * @param mixed $id
      * @return Document
      */
     public function setId($id)
     {
         $this->id = $id;
         return $this;
     }

     /**
      * @return mixed
      */
     public function getName()
     {
         return $this->name;
     }

     /**
      * @param mixed $name
      * @return Document
      */
     public function setName($name)
     {
         $this->name = $name;
         return $this;
     }

     /**
      * @return mixed
      */
     public function getMimeType()
     {
         return $this->mimeType;
     }

     /**
      * @param mixed $mimeType
      * @return Document
      */
     public function setMimeType($mimeType)
     {
         $this->mimeType = $mimeType;
         return $this;
     }

     /**
      * @return UploadedFile
      */
     public function getFile()
     {
         return $this->file;
     }

     /**
      * @param mixed $file
      * @return Document
      */
     public function setFile(UploadedFile $file)
     {
         $this->file = $file;
         // Trigger dirty state
         $this->mimeType = null;
         return $this;
     }
 }