<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 */
class ActivityType{
    /**
     * @ORM\Column(type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     *
     * @Assert\NotNull
     * @Assert\NotBlank
     */
    protected $label;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\TaxTypeRate", mappedBy="activityType")
     */
    protected $taxTypeRate;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return ActivityType
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param mixed $label
     * @return ActivityType
     */
    public function setLabel($label)
    {
        $this->label = $label;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTaxTypeRate()
    {
        return $this->taxTypeRate;
    }

    /**
     * @param mixed $taxTypeRate
     * @return ActivityType
     */
    public function setTaxTypeRate($taxTypeRate)
    {
        $this->taxTypeRate = $taxTypeRate;
        return $this;
    }

    public function __toString()
    {
        return $this->getLabel();
    }
}