<?php
namespace AppBundle\Entity;

use AppBundle\Annotation\UserAware;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 *
 * @UserAware(property="user")
 */
class Estimate
{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="bigint")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @Assert\Length(max=255)
     */
    protected $object;

    /**
     * @ORM\Column(type="date")
     *
     * @Assert\Date()
     */
    protected $createDate;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Customer", inversedBy="estimate")
     *
     * @Assert\NotNull()
     */
    protected $customer;

    /**
     * @ORM\Column(type="date")
     *
     * @Assert\Date()
     */
    protected $validUntil;

    /**
     * @ORM\Column(type="decimal")
     */
    protected $paymentPeriod;


    /**
     * @ORM\Column(type="decimal")
     */
    protected $latePenalty;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $status;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\EstimateLine", mappedBy="estimate", cascade={ "persist" })
     */
    protected $lines;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $isPaid;

    /**
     * @ORM\Column(type="decimal")
     */
    protected $priceTotal;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     */
    protected $user;

    public function __construct()
    {
        $this->createDate = new \DateTime();
        $this->validUntil = new \DateTime();
        $this->lines = new ArrayCollection();
        $this->isPaid = false;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getObject()
    {
        return $this->object;
    }

    /**
     * @param mixed $object
     */
    public function setObject($object)
    {
        $this->object = $object;
    }

    /**
     * @return mixed
     */
    public function getCreateDate()
    {
        return $this->createDate;
    }

    /**
     * @param mixed $createDate
     */
    public function setCreateDate($createDate)
    {
        $this->createDate = $createDate;
    }

    /**
     * @return mixed
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @param mixed $customer
     */
    public function setCustomer($customer)
    {
        $this->customer = $customer;
    }

    /**
     * @return mixed
     */
    public function getValidUntil()
    {
        return $this->validUntil;
    }

    /**
     * @param mixed $validUntil
     */
    public function setValidUntil($validUntil)
    {
        $this->validUntil = $validUntil;
    }

    /**
     * @return mixed
     */
    public function getPaymentPeriod()
    {
        return $this->paymentPeriod;
    }

    /**
     * @param mixed $paymentPeriod
     */
    public function setPaymentPeriod($paymentPeriod)
    {
        $this->paymentPeriod = $paymentPeriod;
    }

    /**
     * @return mixed
     */
    public function getLatePenalty()
    {
        return $this->latePenalty;
    }

    /**
     * @param mixed $latePenalty
     */
    public function setLatePenalty($latePenalty)
    {
        $this->latePenalty = $latePenalty;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getLines()
    {
        return $this->lines;
    }

    public function addLine(EstimateLine $line)
    {
        if (!$this->lines->contains($line)) {
            $line->setEstimate($this);
            $this->lines->add($line);
        }

        return $this;
    }

    public function removeLine(EstimateLine $line)
    {
        $line->setEstimate(null);
        $this->lines->removeElement($line);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsPaid()
    {
        return $this->isPaid;
    }

    /**
     * @param mixed $isPaid
     * @return Estimate
     */
    public function setIsPaid($isPaid)
    {
        $this->isPaid = $isPaid;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     * @return Estimate
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    public function getTotal()
    {
        $total = 0;

        foreach ($this->getLines() as $line) {
            $total += $line->getQuantity() * $line->getPriceUnitary();
        }

        return $total;
    }

    /**
     * @Assert\Callback()
     */
    public function validate(ExecutionContextInterface $context, $payload)
    {
        // Check date validUntil
        if ($this->validUntil < $this->createDate) {
            $context->buildViolation('La date de validité ne doit pas être inférieure à la date du devis')
                ->atPath('validUntil')
                ->addViolation();
        }
    }

    /**
     * @ORM\PreFlush
     */
    public function computeTotal()
    {
        $this->priceTotal = 0;
        foreach ($this->getLines() as $line) {
            $this->priceTotal += $line->getPriceUnitary() * $line->getQuantity();
        }
    }

    /**
     * @return mixed
     */
    public function getPriceTotal()
    {
        return $this->priceTotal;
    }

    /**
     * @param mixed $priceTotal
     */
    public function setPriceTotal($priceTotal)
    {
        $this->priceTotal = $priceTotal;
    }
    
    
}