<?php
namespace AppBundle\Entity;

use AppBundle\Annotation\UserAware;
use AppBundle\Entity\Behavior\HasDocumentInterface;
use AppBundle\Entity\Behavior\HasDocumentTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\InvoiceRepository")
 * @ORM\HasLifecycleCallbacks
 *
 * @UserAware(property="user")
 */
class Invoice implements HasDocumentInterface
{
    use HasDocumentTrait;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="bigint")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @Assert\Length(max=255)
     */
    protected $object;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Customer", inversedBy="invoices")
     *
     * @Assert\NotNull()
     */
    protected $customer;

    /**
     * @ORM\Column(type="date")
     *
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    protected $createDate;

    /**
     * @ORM\Column(type="date")
     *
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    protected $deliveryDate;

    /**
     * @ORM\Column(type="date")
     *
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    protected $paymentDeadline;

    /**
     * @ORM\Column(type="date", nullable=true)
     *
     */
    protected $realDatePaiement;

    /**
     * @ORM\Column(type="decimal")
     */
    protected $priceTotal;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\PaymentMethod")
     *
     * @Assert\NotNull()
     */
    protected $paymentMethod;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\InvoiceLine", mappedBy="invoice", cascade={ "persist" })
     *
     *
     */
    protected $lines;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Estimate")
     */
    protected $estimate;

    /**
     * @ORM\ManyToOne(targetEntity="user")
     */
    protected $user;

    public function __construct()
    {
        $this->lines = new ArrayCollection();
        $this->createDate = new \DateTime();
        $this->documents = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @param mixed $customer
     */
    public function setCustomer($customer)
    {
        $this->customer = $customer;
    }

    /**
     * @return mixed
     */
    public function getDeliveryDate()
    {
        return $this->deliveryDate;
    }

    /**
     * @param mixed $deliveryDate
     */
    public function setDeliveryDate($deliveryDate)
    {
        $this->deliveryDate = $deliveryDate;
    }

    /**
     * @return mixed
     */
    public function getPaymentDeadline()
    {
        return $this->paymentDeadline;
    }

    /**
     * @param mixed $paymentDeadline
     */
    public function setPaymentDeadline($paymentDeadline)
    {
        $this->paymentDeadline = $paymentDeadline;
    }

    /**
     * @return mixed
     */
    public function getRealDatePaiement()
    {
        return $this->realDatePaiement;
    }

    /**
     * @param mixed $realDatePaiement
     */
    public function setRealDatePaiement($realDatePaiement)
    {
        $this->realDatePaiement = $realDatePaiement;
    }

    /**
     * @return mixed
     */
    public function getCreateDate()
    {
        return $this->createDate;
    }

    /**
     * @param mixed $createDate
     */
    public function setCreateDate($createDate)
    {
        $this->createDate = $createDate;
    }

    /**
     * @return mixed
     */
    public function getPriceTotal()
    {
        return $this->priceTotal;
    }

    /**
     * @return mixed
     */
    public function getPaymentMethod()
    {
        return $this->paymentMethod;
    }

    /**
     * @param mixed $paymentMethod
     */
    public function setPaymentMethod($paymentMethod)
    {
        $this->paymentMethod = $paymentMethod;
    }

    /**
     * @return mixed
     */
    public function getObject()
    {
        return $this->object;
    }

    /**
     * @param mixed $object
     */
    public function setObject($object)
    {
        $this->object = $object;
    }

    /**
     * @return mixed
     */
    public function getLines()
    {
        return $this->lines;
    }

    public function addLine(InvoiceLine $line){
        if(!$this->lines->contains($line)){
            $line->setInvoice($this);
            $this->lines->add($line);
        }
        return $this;
    }
    
    public function removeLine(InvoiceLine $line){
        $line->setInvoice(null);
        $this->lines->removeElement($line);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEstimate()
    {
        return $this->estimate;
    }

    /**
     * @param mixed $estimate
     * @return Invoice
     */
    public function setEstimate($estimate)
    {
        $this->estimate = $estimate;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     * @return Invoice
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @ORM\PreFlush
     */
    public function computeTotal()
    {
        $this->priceTotal = 0;
        foreach ($this->getLines() as $line) {
            $this->priceTotal += $line->getPriceUnitary() * $line->getQuantity();
        }
    }
}