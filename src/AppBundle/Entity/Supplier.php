<?php


namespace AppBundle\Entity;

use AppBundle\Annotation\UserAware;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 *
 * @UserAware(property="user")
 */
class Supplier
{
    /**
     * @ORM\Column(type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     *
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    protected $lastname;

    /**
     * @ORM\Column(type="string")
     *
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    protected $firstname;

    /**
     * @ORM\Column(type="string")
     *
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    protected $society;

    /**
     * @ORM\Column(type="string")
     *
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    protected $address;

    /**
     * @ORM\Column(type="string")
     *
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @Assert\Regex("/\d{5}/", message="validator.invalid_postcode")
     */
    protected $postCode;

    /**
     * @ORM\Column(type="string")
     *
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    protected $city;

    /**
     * @ORM\Column(type="string")
     *
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    protected $country;

    /**
     * @ORM\Column(type="string")
     *
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @Assert\Email()
     */
    protected $email;

    /**
     * @ORM\Column(type="string")
     *
     * @Assert\NotNull()
     * @Assert\NotBlank()
     *
     * @Assert\Regex(pattern="/\d{10}/", message="invalid.phone")
     */
    protected $phone;

    /**
     * @ORM\Column(type="string")
     *
     * @Assert\Regex(pattern="/\d{10}/", message="invalid.phone")
     */
    protected $fax;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     */
    protected $user;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Buy", mappedBy="supplier", cascade={ "persist" })
     */
    protected $buy;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @param mixed $lastname
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
    }

    /**
     * @return mixed
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * @param mixed $firstname
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
    }

    /**
     * @return mixed
     */
    public function getSociety()
    {
        return $this->society;
    }

    /**
     * @param mixed $society
     */
    public function setSociety($society)
    {
        $this->society = $society;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return mixed
     */
    public function getPostCode()
    {
        return $this->postCode;
    }

    /**
     * @param mixed $postCode
     */
    public function setPostCode($postCode)
    {
        $this->postCode = $postCode;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return mixed
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * @param mixed $fax
     */
    public function setFax($fax)
    {
        $this->fax = $fax;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     * @return Supplier
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBuy()
    {
        return $this->buy;
    }

    /**
     * @param mixed $buy
     */
    public function setBuy($buy)
    {
        $this->buy = $buy;
    }
    

    public function __toString()
    {
        return $this->getFirstname() ." ".$this->getLastname() . " - " . $this->getSociety();
    }
}